<?php

namespace app\controllers;

use app\models\Events;
use app\models\EventsSearch;
use app\models\Organizers;
use Yii;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * EventsController implements the CRUD actions for Events model.
 */
class EventsController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
                'access' => [
                    'class' => AccessControl::className(),
                    'rules' => [
                        [
                            'allow' => true,
                            'roles' => ['@'],
                        ],
                    ]
                ],
            ]
        );
    }

    /**
     * Lists all Events models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new EventsSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * Displays a single Events model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }



    /**
     * Creates a new Events model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Events();
        $organizers = Organizers::find()->all();
        if (!$organizers) {
            \Yii::$app->session->setFlash('error', 'Создайте организатора');

            return $this->redirect("index");
        }
        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Events model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param int $id ID
     *
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        //var_dump($this->request->post()); exit;
        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionList($event_id, $q = null, $id = null){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => []];
        //$str = Yii::$app->request->queryParams["q"];
        //$event_id = Yii::$app->request->queryParams["event_id"];
        if(!is_null($q)){
            $query = new Query();
            $query->select('organizer_id')
                ->from('events_via_organizers')
                ->where(["event_id"=> $event_id]);

            $rows = $query->all();
            $ids = [];
            foreach ($rows as $r){
                $ids[] = $r["organizer_id"];
            }

            $organizers = Organizers::find()
                ->andWhere(['like', 'soname', $q . '%', false])
                ->orWhere(['like', 'name', $q . '%', false])
                ->orWhere(['like', 'second_name', $q . '%', false])
                ->andWhere(['NOT', ['id' => $ids]])->all();

            if($organizers){
                foreach($organizers as &$organizer){
                    $organizer->name = $organizer->name.' '. $organizer->second_name. ' '.$organizer->soname;
                    $out['results'][] = ['id' => $organizer->id, 'text' => $organizer->name];
                }
                //$organizers  = ArrayHelper::map($organizers, "id", "name");
                //$out['results'] = [['id' => 1, 'text' => 'test'], ['id' => 2, 'text' => 'ророро']];
                //print_r($out);//convert it as arrays
                //exit;


            }

        }  elseif ($id > 0) {
            $organizers = ['id' => $id, 'name' => Organizers::findOne($id)->name];
        }


        return $out;
    }

    /**
     * Deletes an existing Events model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->delete();


        return $this->redirect(['index']);
    }





    /**
     * Finds the Events model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return Events the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Events::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
