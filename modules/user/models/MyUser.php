<?php

namespace app\modules\user\models;

use yii\helpers\ArrayHelper;
use yii\web\IdentityInterface;
use app\components\UserHelper;
use Yii;

/**
 * This is the model class for table "my_user".
 *
 * @property int $id
 * @property int $created_at
 * @property int $updated_at
 * @property string $username
 * @property string|null $auth_key
 * @property string $password_hash
 * @property string $email
 * @property int $status
 */
class MyUser extends \yii\db\ActiveRecord implements IdentityInterface
{
    /**
     * {@inheritdoc}
     */
    public static function roles()
    {
        return [
            UserHelper::ROLE_USER => Yii::t('app', 'User'),
            UserHelper::ROLE_ADMIN => Yii::t('app', 'Admin'),
        ];
    }
        /**
     * Название роли
     * @param int $id
     * @return mixed|null
     */

    public function getRoleName(int $id)
    {
        $list = self::roles();
        return $list[$id] ?? null;
    }

    public function isAdmin()
    {
        return ($this->role == UserHelper::ROLE_ADMIN);
    }

    public function isUser()
    {
        return ($this->role == UserHelper::ROLE_USER);
    }

    public function afterSave($isInsert, $changedOldAttributes)
    {
        return parent::afterSave($isInsert, $changedOldAttributes);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::find()
            ->where(['id' => (string)$token->getClaim('uid')])
            ->one();
    }


    public static function checkExpire($token)
    {
        $current = time();
        $expire = $token->getClaim('exp');
        $diff = $current - $expire;
        if ($diff >=0) {
            return false;
        }
        return true;
    }
    public function getStatusName()
    {
        return ArrayHelper::getValue(self::getStatusesArray(), $this->status);
    }
     /**
     * {@inheritdoc}
     */
    public static function getStatusesArray()
    {
        return [
            UserHelper::STATUS_BLOCKED => 'Заблокирован',
            UserHelper::STATUS_ACTIVE => 'Активен',
            UserHelper::STATUS_WAIT => 'Ожидает подтверждения',
        ];
    }
     /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'my_user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['username', 'match', 'pattern' => '#^[\w_-]+$#is'],
            ['username', 'unique', 'targetClass' => self::className(),
             'message' => 'This username has already been taken.'],
            ['username', 'string', 'min' => 2, 'max' => 255],
            [['created_at', 'updated_at', 'username', 'password_hash', 'email'], 'required'],
            [['created_at', 'updated_at', 'status'], 'integer'],
            [[ 'password_hash', 'email'],
             'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
            ['email', 'unique', 'targetClass' => self::className(),
             'message' => 'This email address has already been taken.'],
            ['status', 'default', 'value' => UserHelper::STATUS_ACTIVE],
            ['status', 'in', 'range' => array_keys(self::getStatusesArray())],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'created_at' => Yii::t('app', 'Создан'),
            'updated_at' => Yii::t('app', 'Обновлен'),
            'username' => Yii::t('app', 'Имя пользователя'),
            'auth_key' => Yii::t('app', 'Auth Key'),
            'password_hash' => Yii::t('app', 'Password Hash'),
            'email' => Yii::t('app', 'Email'),
            'status' => Yii::t('app', 'Статус'),
        ];
    }

    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => UserHelper::STATUS_ACTIVE]);
    }



    public function getId()
    {
        return $this->getPrimaryKey();
    }

    public function getAuthKey()
    {
        return $this->auth_key;
    }

    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }
    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username]);
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }
    /**
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                $this->generateAuthKey();
            }
            return true;
        }
        return false;
    }
}
