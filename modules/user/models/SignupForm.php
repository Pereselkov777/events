<?php

namespace app\modules\user\models;

use app\components\UserHelper;
use yii\base\Model;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $username;
    public $email;
    public $password;

    public function rules()
    {
        return [
            ['username', 'filter', 'filter' => 'trim'],
            ['username', 'required'],
            ['username', 'match', 'pattern' => '#^[\w_-]+$#i'],
            ['username', 'unique', 'targetClass' => MyUser::className(),
             'message' => 'This username has already been taken.'
            ],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'unique', 'targetClass' => MyUser::className(),
             'message' => 'This email address has already been taken.'
            ],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],
        ];
    }

    /**
     * Signs user up.
     *
     * @return MyUser|null the saved model or null if saving fails
     */
    public function signup()
    {
        if ($this->validate()) {
            $user = new MyUser();
            $user->username = \yii\helpers\Html::encode($this->username);
            $user->email = \yii\helpers\Html::encode($this->email);
            $user->setPassword(\yii\helpers\Html::encode($this->password));
            $user->status = UserHelper::STATUS_ACTIVE;
            $user->generateAuthKey();
            $user->created_at = time();
            $user->updated_at = time();
            if (!$user->save()) {
                var_dump($user->getErrors());
                exit();
            }
            if ($user->save()) {
                return $user;
            }
        }

        return null;
    }
}
