<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%todo}}`.
 */
class m230127_190223_create_organizers_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%organizers}}', [
            'id'       => $this->primaryKey(),
            "name"    => $this->string(255)->notNull(),
            "second_name" => $this->string(255)->notNull(),
            "soname"  => $this->string(255)->notNull(),
            "email"  => $this->string(255)->notNull(),
            "phone"     => $this->string(10)->null(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%organizers}}');
        return true;
    }
}
