<?php


use yii\db\Migration;

/**
 * Handles the creation of table `{{%my_user}}`.
 */
class m220518_121900_create_my_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%my_user}}', [
            'id'            => $this->primaryKey(),
            'created_at'    => $this->integer(10)->notNull(),
            'updated_at'    => $this->integer(10)->notNull(),
            'username'      => $this->string(255)->notNull(),
            'auth_key'      => $this->string(255)->null(),
            'password_hash' => $this->string(255)->null(),
            'email'         => $this->string(255)->notNull(),
            'status'        => $this->integer(10)->notNull()->defaultValue(0),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%my_user}}');
    }
}
