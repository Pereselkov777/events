<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%events}}`.
 */
class m240527_133733_create_events_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%events}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull()->comment('Название'),
            'description' => $this->text()->null()->comment('Описание'),
            'created_at' => $this->dateTime()->null()->comment('Создано'),

        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%events}}');
    }
}
