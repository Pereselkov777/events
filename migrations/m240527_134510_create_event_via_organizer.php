<?php

use yii\db\Migration;

/**
 * Class m240527_134510_create_event_via_organizer
 */
class m240527_134510_create_event_via_organizer extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%events_via_organizers}}', [
            'event_id' => $this->integer()->notNull(),
            'organizer_id' => $this->integer()->notNull(),
        ]);
        $this->addPrimaryKey('idx_primary', '{{%events_via_organizers}}', ['event_id','organizer_id']);
        $this->createIndex('idx_event_id', '{{%events_via_organizers}}', 'event_id');
        $this->createIndex('idx_organizer_id', '{{%events_via_organizers}}', 'organizer_id');

        $this->addForeignKey('fg_events_via_organizers_event_id', '{{%events_via_organizers}}', 'event_id', '{{%events}}', 'id', 'CASCADE', null );
        $this->addForeignKey('fg_events_via_organizers_organizer_id','{{%events_via_organizers}}', 'organizer_id', '{{%organizers}}', 'id', 'CASCADE', null );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $table         = Yii::$app->db->schema->getTableSchema('events_via_organizers');
        $uniqueIndexes = Yii::$app->db->schema->findUniqueIndexes($table);
        $hasIndex      = \yii\helpers\ArrayHelper::keyExists('fg_events_via_organizers_event_id', $uniqueIndexes);


        if($hasIndex){
            $this->dropIndex(
                'fg_events_via_organizers_event_id',
                'events_via_organizers',
            );
        }
        $hasIndex      = \yii\helpers\ArrayHelper::keyExists('fg_events_via_organizers_organizer_id', $uniqueIndexes);
        if($hasIndex){
            $this->dropIndex(
                'fg_events_via_organizers_organizer_id',
                'events_via_organizers',
            );
        }
       /*  $this->dropIndex(
            'idx_event_id',
            'events_via_organizers',
        );
        $this->dropIndex(
            'idx_organizer_id',
            'events_via_organizers',
        ); */

        $this->dropTable('{{%events_via_organizers}}');
        return true;
    }
}
