<?php

namespace app\components;

/**
 * Class UserHelper
 * @package app\components
 */
class UserHelper
{
    public const STATUS_BLOCKED = 0;
    public const STATUS_ACTIVE  = 1;
    public const STATUS_WAIT    = 2;

    public const ROLE_USER  = 1;
    public const ROLE_ADMIN = 10;
}
