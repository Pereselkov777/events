<?php


namespace unit\models;

use app\models\Organizers;

/**
 * Class OrganizerTest
 *
 * @package unit\models
 */
class OrganizerTest extends \Codeception\Test\Unit
{
    public function testValidationName()
    {
        $organizer = new Organizers();

        $this->assertFalse($organizer->validate(['name']));
        $this->assertFalse($organizer->validate(['second_name']));
        $this->assertFalse($organizer->validate(['soname']));
    }

    public function testValidationEmailPhone()
    {
        $organizer = new Organizers();

        $this->assertFalse($organizer->validate(['email']));
        $this->assertTrue($organizer->validate(['phone']));
    }

    public function testSave()
    {
        $organizer = new Organizers();
        $organizer->name = "Name";
        $organizer->second_name = "SecondName";
        $organizer->soname = "Soname";
        $organizer->email = "test@test.com";
        $organizer->save();
        $errors = $organizer->getErrors();

        $this->assertTrue(count($errors) === 0);
    }
}
