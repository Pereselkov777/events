<?php

namespace tests\unit\models;

use app\models\UsersItem;
use app\modules\user\models\MyUser;

class MyUserTest extends \Codeception\Test\Unit
{
    public function testValidation()
    {
        $user = new MyUser();

        $user->username = null;
        $this->assertFalse($user->validate(['username']));
        $user->username = 'admin1';
        $this->assertTrue($user->validate(['username']));
    }

    public function testUser()
    {
        $user = \Codeception\Stub::make('app\modules\user\models\MyUser', ['username' => 'John', 'password' => '123456']);
        $user->find(1);
        $this->assertTrue($user instanceof MyUser);
    }
}
