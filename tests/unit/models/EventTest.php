<?php


namespace unit\models;

use app\models\Events;

/**
 * Class EventTest
 *
 * @package unit\models
 */
class EventTest extends \Codeception\Test\Unit
{
    public function testValidationName()
    {
        $event = new Events();
        $event->created_at = date('Y-m-d H:i:s');
        $this->assertFalse($event->validate(['name']));
    }

    public function testValidationDate()
    {
        $event = new Events();
        $event->name =  "Test name";
        $this->assertTrue($event->validate(['name']));
        $this->assertTrue($event->validate(['created_at']));
    }

}
