<?php

/** @var yii\web\View $this */
/** @var string $content */

use app\assets\AppAsset;
use app\widgets\Alert;
use yii\bootstrap5\Breadcrumbs;
use yii\bootstrap5\Html;
use yii\bootstrap5\Nav;
use yii\bootstrap5\NavBar;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="h-100">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="d-flex flex-column h-100">
<?php $this->beginBody() ?>

<header>
    <?php
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar navbar-expand-md navbar-dark bg-dark fixed-top',
        ],
    ]);
echo Nav::widget([
    'options' => ['class' => 'navbar-nav'],
    'items' => [
            //['label' => 'SignUp', 'url' => ['/user/auth/signup'], ]
             ['label' => 'SignUp', 'url' =>  ['/user/auth/signup'],'visible' => Yii::$app->user->isGuest, ],
            ['label' => 'SignIn', 'url' =>  ['/user/auth/login'],'visible' => Yii::$app->user->isGuest, ],
            ['label' => 'Logout', 'url' =>  ['/user/auth/logout'],'visible' => !Yii::$app->user->isGuest, ],

            ['label' => 'События', 'url' =>  ['/events/index'], 'visible' => !Yii::$app->user->isGuest,  ],
            ['label' => 'Организаторы', 'url' =>  ['/organizers/index'], 'visible' => !Yii::$app->user->isGuest,   ],
    ],
]);
NavBar::end();
?>
</header>

<main role="main" class="flex-shrink-0">
    <div class="container">
        <?= Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
    ]) ?>

        <?php if(Yii::$app->session->getFlash("error")):?>
            <div class="info">
                <?php echo Yii::$app->session->getFlash("error"); ?>
            </div>
        <?php endif; ?>

        <?= $content ?>
    </div>
</main>

<footer class="footer mt-auto py-3 text-muted">
    <div class="container">
        <p class="float-left">&copy; My Company <?= date('Y') ?></p>
        <p class="float-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
