<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use kartik\select2\Select2;

/** @var yii\web\View $this */
/** @var app\models\Events $model */
/** @var yii\widgets\ActiveForm $form */

?>

<div class="events-form">

    <?php $form = ActiveForm::begin();
    $model->organizersArr =  array_keys($model->getCurrent());
    ?>
    <?=


    // Tagging support Multiple
   // initial value
    $form->field($model, 'organizersArr')->widget(Select2::classname(), [
        'data' => $model->getAvailable(),
        'bsVersion' => '5.x',
        'options' => ['placeholder' => 'Select a color ...', 'multiple' => true],
        'pluginOptions' => [
            'tags' => true,
            'tokenSeparators' => [',', ' '],
            'maximumInputLength' => 10
        ],
    ])->label('Tag Multiple');

    /* $form->field($model, 'organizersArr')->widget(Select2::classname(), [
             'initValueText' => array_values($model->getCurrent()),
        'bsVersion' => '5.x',
        'options' => ['multiple'=>true,
            'placeholder' => 'Выберите организатора ...',

            'value' => array_keys($model->getCurrent()),
            ],
        'pluginOptions' => [
            'allowClear' => true,
            'minimumInputLength' => 3,
            'language' => [
                'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
            ],
            'ajax' => [
                'url' => \yii\helpers\Url::to(['/events/list?event_id=' . $model->id]),
                'dataType' => 'json',
                'data' => new JsExpression('function(params) { return {q:params.term}; }')
            ],
            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
            'templateResult' => new JsExpression('function(data) {
             return data.text; }'),
            'templateSelection' => new JsExpression('function (data) { return data.text; }'),
        ],
    ]); */



    ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>


    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>


    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
