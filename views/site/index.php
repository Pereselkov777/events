<?php

use app\models\Events;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\grid\GridView;

/** @var yii\web\View $this */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent">
        <h1 class="display-4">Congratulations!</h1>

        <p class="lead">You have successfully created your Yii-powered application.</p>

        <p><a class="btn btn-lg btn-success" href="http://www.yiiframework.com">Get started with Yii</a></p>
    </div>

    <div class="body-content">

        <div class="events-index">

            <h1><?= Html::encode($this->title) ?></h1>

            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

            <?= GridView::widget([
                'dataProvider' => $dataProvider,

                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    'id',
                    'name',
                    'description:ntext',
                    'created_at',
                    [
                        'attribute' => 'organizers',
                        'value' => function ($model) {
                            $array = $model->getOrganizers()->asArray()->all();

                            $str = '';
                            if (!empty($array)) {
                                foreach ($array as $organizer) {
                                    $str = $str . $organizer["soname"] . ', ';
                                }
                            }
                            return $str;
                        },
                    ],
                ],
            ]); ?>


        </div>

    </div>
</div>
