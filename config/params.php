<?php


return [
    'app_url' => getenv('APP_URL'),
    'domain' => "http://yourdomain.com",
    'bsDependencyEnabled' => false,
];
