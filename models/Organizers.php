<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "organizers".
 *
 * @property int $id
 * @property string $name
 * @property string $second_name
 * @property string $soname
 * @property string $email
 * @property int|null $phone
 */
class Organizers extends \yii\db\ActiveRecord
{

    public $full_name;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'organizers';
    }

    public function beforeDelete()
    {
       // $q = 'delete from {{%event_via_organizer}} where organizer_id ='.(int)$this->id;
       // Yii::$app->db->createCommand($q)->execute();
        return parent::beforeDelete(); // TODO: Change the autogenerated stub
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'second_name', 'soname', 'email'], 'required'],
            ['phone', 'match', 'pattern'=>"/^[0-9]{3}[0-9]{3}[0-9]{2}[0-9]{2}$/",'message'=> 'Not Correct Format - mobile number should have 10 digits.'],
            [['email'], 'email'],
            [['phone'], 'string', 'min' => 9, 'max' =>10],
            [['name', 'second_name', 'soname', 'email'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'second_name' => 'Second Name',
            'soname' => 'Soname',
            'email' => 'Email',
            'phone' => 'Phone',
        ];
    }

    public function getEvents()
    {
        return $this->hasMany(Events::class, ['id' => 'event_id'])->viaTable('{{%events_via_organizers}}', ['organizer_id' => 'id']);
    }
}
